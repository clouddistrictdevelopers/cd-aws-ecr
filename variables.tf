variable "repo_name" {
  type = string
  description = "Name of the repository"
}
variable "tags" {
  type        = map(string)
  default     = {}
  description = "Default tags"
}

variable "scan_on_push" {
  type        = bool
  default     = false
  description = "Indicates whether images are scanned after being pushed to the repository (true) or not scanned (false)"
}

variable "image_tag_mutability" {
  type = string
  default = "MUTABLE"
  description = "The tag mutability setting for the repository. Defaults to `MUTABLE`."
}
